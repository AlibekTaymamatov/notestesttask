import React from 'react';
import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Main from './Components/Main/Main';
import AddNote from './Components/AddNote/AddNote';
import EditNote from './Components/EditNote/EditNote';

const App = () => {
  return (
    <div className="dark-mode">
    <Router>
      <h1 className='titleApp'>NOTES</h1>
        <Routes>
          <Route path="/"  element={<Main />} />
          <Route path="/addNote"  element={<AddNote />} />
          <Route path="/:id"  element={<EditNote />} />
        </Routes>
    </Router>
    </div>
  );
}

export default App;
