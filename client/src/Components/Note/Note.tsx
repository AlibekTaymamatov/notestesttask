import React from 'react';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';

export interface NoteProps {
    id: number;
    titleNote: string;
    description: string;
    dateOfCreation: Date;
  }

const Note = (props: NoteProps) => {
    return(
        <div>
             <Card sx={{ minxWidth: 800, margin: 3 }} className="note">
                <CardHeader title = {props.titleNote} />
                <CardContent>
                    <Typography variant='body2' color = "textSecondary">
                        {props.description}
                    </Typography>
                </CardContent>
                <CardActions>
                <Typography color = "textSecondary">
                        {new Date(props.dateOfCreation).toDateString()}
                    </Typography>
                </CardActions>
            </Card>
        </div>
    )
};

export default Note;