import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns'
import TextareaAutosize from '@mui/base/TextareaAutosize';
import Button from '@mui/material/Button';
import DatePicker  from '@mui/lab/DatePicker';
import Grid from '@mui/material/Grid';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import { useStyles } from "../../Components/AddNote/AddNote";
import { getItemDetails, updateNote, deleteNote } from '../../Components/ServiceAPI/ServiceAPI';

const EditNote = () => {

    var classes = useStyles();
    const { id } = useParams() as any;

    const [item, setItem] = useState<{id: number, titleNote: string, description: string, dateOfCreation: string | Date | null }>({
        id: 0,
        titleNote: '',
        description: '',
        dateOfCreation: '',
    });

    const fetchItem = async () => {
        try {
          const data = await getItemDetails(id);
          setItem(data);
        } catch (err: any) {
          throw Error(err);
        }
    };

    const updateItem = async () => {
        try {
        const data = await updateNote(item)
        console.log(data)
        } catch (err: any) {
          throw Error(err);
        }
    };

    const deleteItem = async () => {
        try {
          const data = await deleteNote(item.id);
          console.log(data)
        } catch (err: any) {
          throw Error(err);
        }
    };

    useEffect(() => {
        fetchItem();
    }, []);

    return (
        <>
            <Grid>
                <Card className={classes.card} sx={{ minWidth: window.innerWidth/2 , minHeight: window.innerHeight/2 }}>
                    <CardHeader title = { 
                        <TextField
                            required
                            id="outlined-required"
                            label="Title"
                            value={item.titleNote}
                            onChange={(newValue)=>setItem ({...item, titleNote: newValue.target.value})}
                            fullWidth 
                        />}
                    />
                    <CardContent>
                        <TextareaAutosize
                            maxRows={4}
                            aria-label="empty textarea"
                            placeholder="My notes"
                            style={{ width: 1000 , height: 400}}
                            className={classes.textarea}
                            value={item.description}
                            onChange={(newValue)=>setItem ({...item, description: newValue.target.value})}
                        />
                    </CardContent>
                    <CardActions className={classes.cardActions}>
                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                        <DatePicker
                            value={item.dateOfCreation ? new Date(item.dateOfCreation) : ''}
                            onChange={(newValue)=>setItem ({...item, dateOfCreation: newValue})}
                            renderInput={(params) => <TextField {...params} />}
                        />
                    </LocalizationProvider>
                        <CardActions>
                            <Link to="/" className='item__link'>
                                <Button 
                                    variant= 'contained' 
                                    color="primary" 
                                    style={{ backgroundColor: '#ff0000', color: 'white', width: 100, height: 40 }}
                                    sx={{margin: 2}}
                                    onClick={deleteItem}
                                >
                                    Delete
                                </Button>
                                <Button 
                                    variant= 'contained' 
                                    color="primary" 
                                    style={{ backgroundColor: '#ffffff', color: 'black', width: 100, height: 40 }}
                                    onClick={updateItem}
                                >
                                    Save
                                </Button>
                            </Link>
                        </CardActions>
                    </CardActions>
                </Card>
            </Grid>
        </>
    );
}

export default EditNote;
