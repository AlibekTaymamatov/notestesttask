import { BASIC_SERVER_URL } from '../../const'

async function getNotes() {
  const response = await fetch(`${BASIC_SERVER_URL}/api/notes/`);
  const data = await response.json();
  return data;
}

async function getItemDetails(id: string) {
  const response = await fetch(`${BASIC_SERVER_URL}/api/notes/${id}`);
  const data = await response.json();
  return data;
}

async function deleteNote(id: number) {
  const response = await fetch(`${BASIC_SERVER_URL}/api/notes/${id}`, {
    method: 'DELETE',
  });
  const data = await response.status;
  if (data != 204) {
    throw new Error();
  }
  return data;
}

async function updateNote(item: {id: number, titleNote: string, description: string, dateOfCreation: string | Date | null }) {

  var parms = new URLSearchParams();
  parms.append('id', item.id.toString());
  parms.append('titleNote', item.titleNote.toString());
  parms.append('description', item.description.toString());
  parms.append('dateOfCreation', item.dateOfCreation!.toLocaleString());

  const response = await fetch(`${BASIC_SERVER_URL}/api/notes/`,
  {
    method: "PUT",
    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
    body: parms.toString(),
  });

  const data = await response.json();
  if (!data == null) {
    const message = `An error has occured: ${data.status} - ${data.statusText}`;
    throw new Error(message);
  }
  return data;
}

async function createNote(item: {titleNote: string, description: string, dateOfCreation: string | Date | null }) {

  var parms = new URLSearchParams();
  parms.append('titleNote', item.titleNote.toString());
  parms.append('description', item.description.toString());
  parms.append('dateOfCreation', item.dateOfCreation!.toLocaleString());

  const response = await fetch(`${BASIC_SERVER_URL}/api/notes/`,
  {
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
    body: parms.toString(),
  });

  const data = await response.json();
  if (!data == null) {
    const message = `An error has occured: ${data.status} - ${data.statusText}`;
    throw new Error(message);
  }
  return data;
}

export { getNotes, getItemDetails, updateNote, createNote, deleteNote };