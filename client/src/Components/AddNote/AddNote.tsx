import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import TextField from '@mui/material/TextField';
import TextareaAutosize from '@mui/base/TextareaAutosize';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import Button from '@mui/material/Button';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker  from '@mui/lab/DatePicker';
import Grid from '@mui/material/Grid';
import { makeStyles } from "@mui/styles";
import { createNote } from '../../Components/ServiceAPI/ServiceAPI';


export const useStyles = makeStyles({
    card: {
    top: '50%',
    left: '50%',
    borderRadius: 10,
    position: 'absolute',
    transform: 'translate(-50%, -50%)',
    backgroundSize: '200%',
    transition: '0.6s',
    backgroundImage: 'linear-gradient(45deg, #37ecba, #72afd3)',
    '&:hover': {
      backgroundPosition: 'right'
    },
    },
    cardActions: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',

    },
    textarea: {
        border: 0,
        background: 0,
        resize: "none",
        outline: "none",
      },
  });

function AddNote () {

    var classes = useStyles();

    const [item, setItem] = useState<{titleNote: string, description: string, dateOfCreation: string | Date | null }>({
        titleNote: '',
        description: '',
        dateOfCreation: new Date(),
    });
    
    const createItem = async () => {
        try {
        const data = await createNote(item)
        console.log(data)
        } catch (err: any) {
          throw Error(err);
        }
    };

    return(
        <div>
            <Grid>
                <Card className={classes.card} sx={{ minWidth: window.innerWidth/2 , minHeight: window.innerHeight/2 }}>
                    <CardHeader title = { 
                        <TextField
                            required
                            id="outlined-required"
                            label="Title"
                            onChange={(newValue)=>setItem ({...item, titleNote: newValue.target.value})}
                            fullWidth 
                        />}
                    />
                    <CardContent>
                        <TextareaAutosize
                            maxRows={4}
                            aria-label="empty textarea"
                            placeholder="My notes"
                            style={{ width: 1000 , height: 400}}
                            className={classes.textarea}
                            onChange={(newValue)=>setItem ({...item, description: newValue.target.value})}
                        />
                    </CardContent>
                    <CardActions className={classes.cardActions}>
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                        <DatePicker
                            disabled
                            value={new Date()}
                            onChange={(newValue)=>setItem ({...item, dateOfCreation: newValue})}
                            renderInput={(params) => <TextField {...params} helperText={null} />}
                        />
                        </LocalizationProvider>
                        <CardActions>
                            <Link to="/" className='item__link'>
                                <Button 
                                    variant= 'contained' 
                                    color="primary" 
                                    style={{ backgroundColor: '#ff0000', color: 'white', width: 100, height: 40 }}
                                    sx={{margin: 2}}
                                >
                                    Close
                                </Button>
                                <Button 
                                    variant= 'contained' 
                                    color="primary" 
                                    style={{ backgroundColor: '#ffffff', color: 'black', width: 100, height: 40 }}
                                    onClick={createItem}
                                >
                                    Save
                                </Button>
                             </Link>
                        </CardActions>
                    </CardActions>
                </Card>
            </Grid>
        </div>
    )
};

export default AddNote;
