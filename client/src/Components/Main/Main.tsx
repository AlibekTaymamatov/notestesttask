import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import AddIcon from '@mui/icons-material/Add';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Note from '../../Components/Note/Note';
import { getNotes } from '../../Components/ServiceAPI/ServiceAPI';
import { NoteProps } from '../../Components/Note/Note';

function Main() {

    const [notes, setNotes] = useState([]);

    useEffect(() => {  
        const loadData = async () => {
            try {
                const data = await getNotes();
                setNotes(data);
            }
            catch (err: any) {
                throw Error(err);
            }
        }

        loadData().catch((err) => console.error(err));
    },[])

    return (
        <>
            <Box>
                <Grid container spacing={3}>
                    {notes.map((note: NoteProps) => (
                        <Grid item key={note.id} xs={12} md={6} lg={4}>
                            <Link className='item__link' to={`/${note.id}`}>
                            <Note
                                id={note.id}
                                titleNote={note.titleNote}
                                description={note.description}
                                dateOfCreation={note.dateOfCreation}
                            />
                            </Link>
                        </Grid>
                    ))}
                </Grid>
            </Box>
            <div>{ButtonAdd()}</div>
        </>
    );
}

const ButtonAdd = () => {
    return (
        <>
            <Link to="/addNote" className="positionButton">
                <AddIcon sx={{ fontSize: 70, color: 'white' }}>
                </AddIcon>
            </Link>
        </>
    );
}

export default Main;
