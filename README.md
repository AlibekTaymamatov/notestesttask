# App Note

# Start project

Server build:

- Entity Framework ver. 5.0.6 on `https://www.nuget.org/packages/Microsoft.EntityFrameworkCore.Tools/5.0.6`
- Microsoft SQL Server database provider for Entity Framework Core. 5.0.1
- Recommened IDE for server build Visual Studio ver. 2019
- Migration db: `update-database`

![image](image/update-database.png)

Client build:

- React 17 + TS + Material UI
- Recommened IDE Visual Studio
- `cd client`, run `npm install` and then `npm start` then open `http://localhost:3000`


# Application features

Реализация базовых функций CRUD

![image](image/swagger.png)

# Example
Add Note

![image](image/AddNote.gif)

Edit Note

![image](image/EditNote.gif)

Delete Note

![image](image/DeleteNote.gif)
