﻿namespace Application.ViewModels
{
    using System;
    using Domain.Models;

    public class NoteDto
    {
        public NoteDto(Note note)
        {
            this.Id = note.Id;
            this.TitleNote = note.TitleNote;
            this.Description = note.Description;
            this.DateOfCreation = note.DateOfCreation;
        }

        public int Id { get; set; }

        public string TitleNote { get; set; }

        public string Description { get; set; }

        public DateTime DateOfCreation { get; set; }
    }
}
