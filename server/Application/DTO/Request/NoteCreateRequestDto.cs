﻿namespace Application.DTO.Request
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using Application.Interfaces;
    using Domain.Models;

    public class NoteCreateRequestDto : IDtoMapper<Note>
    {
        [Required]
        [StringLength(50, ErrorMessage = "Invalid name length")]
        public string TitleNote { get; set; }

        [MaxLength(200, ErrorMessage = "Invalid description length")]
        public string Description { get; set; }

        [Required]
        public DateTime DateOfCreation { get; set; }

        public Note ToModel()
        {
            return new Note()
            {
                TitleNote = this.TitleNote,
                Description = this.Description,
                DateOfCreation = this.DateOfCreation,
            };
        }
    }
}