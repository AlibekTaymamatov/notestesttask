﻿namespace Application.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using Application.DTO.Request;
    using Application.Interfaces;
    using Application.ViewModels;
    using Domain.Models;
    using Domain.Repository;

    public class NoteService : INoteService
    {
        private INoteRepository _noteRepository;

        public NoteService(INoteRepository noteRepository)
        {
            _noteRepository = noteRepository;
        }

        public List<NoteDto> GetNotes()
        {
            var books = _noteRepository.GetNotes();
            return books.Select(i => new NoteDto(i)).ToList();
        }

        public NoteDto GetNote(int id)
        {
            Note book = _noteRepository.GetNote(id);
            if (book is null)
            {
                return null;
            }

            return new NoteDto(book);
        }

        public NoteDto InsetNote(NoteCreateRequestDto noteDto)
        {
            Note note = noteDto.ToModel();
            return new NoteDto(_noteRepository.InsertNote(note));
        }

        public NoteDto UpdateNote(NoteUpdateRequestDto noteDto)
        {
            Note note = noteDto.ToModel();
            note = _noteRepository.UpdateNote(note);

            if (note is null)
            {
                return null;
            }

            return new NoteDto(note);
        }

        public void DeleteNote(int id)
        {
           _noteRepository.DeleteNote(id);
        }
    }
}
