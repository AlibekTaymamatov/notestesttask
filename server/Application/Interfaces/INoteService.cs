﻿namespace Application.Interfaces
{
    using System.Collections.Generic;
    using Application.DTO.Request;
    using Application.ViewModels;

    public interface INoteService
    {
        List<NoteDto> GetNotes();

        NoteDto GetNote(int id);

        NoteDto InsetNote(NoteCreateRequestDto noteDto);

        NoteDto UpdateNote(NoteUpdateRequestDto noteDto);

        void DeleteNote(int id);
    }
}
