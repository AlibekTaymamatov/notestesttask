﻿namespace WebApi.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Application.DTO.Request;
    using Application.Interfaces;
    using Application.ViewModels;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [ApiController]
    [Route("api/[controller]")]
    public class NotesController : ControllerBase
    {
        private readonly ILogger<NotesController> logger;
        private INoteService _noteService;

        public NotesController(ILogger<NotesController> logger, INoteService noteService)
        {
            this.logger = logger;
            _noteService = noteService;
        }

        [HttpGet]
        public ActionResult<List<NoteDto>> Get()
        {
            try
            {
                return Ok(_noteService.GetNotes());
            }
            catch (Exception error)
            {
                return BadRequest(error.Message);
            }
        }

        [HttpGet("{id}")]
        public ActionResult<NoteDto> Get([FromRoute] int id)
        {
            NoteDto book = _noteService.GetNote(id);
            return book != null ? Ok(book) : NotFound();
        }

        [HttpPost]
        public ActionResult<NoteDto> Insert([FromForm] NoteCreateRequestDto note)
        {
            var results = new List<ValidationResult>();
            var context = new ValidationContext(note);
            if (!Validator.TryValidateObject(note, context, results, true))
            {
                return BadRequest(results.ToArray());
            }

            NoteDto noteDto = _noteService.InsetNote(note);
            return this.Created(new Uri(noteDto.Id.ToString(), UriKind.Relative), noteDto);
        }

        [HttpPut]
        public ActionResult<NoteDto> Update([FromForm] NoteUpdateRequestDto note)
        {
            var results = new List<ValidationResult>();
            var context = new ValidationContext(note);
            if (!Validator.TryValidateObject(note, context, results, true))
            {
                return BadRequest(results.ToArray());
            }

            NoteDto noteUpd = _noteService.UpdateNote(note);
            return noteUpd != null ? Ok(noteUpd) : NotFound();
        }

        [HttpDelete("{id}")]
        public ActionResult<NoteDto> Delete([FromRoute] int id)
        {
            try
            {
                _noteService.DeleteNote(id);
                return NoContent();
            }
            catch (ArgumentNullException)
            {
                return NotFound();
            }
            catch (Exception err)
            {
                return Problem(err.Message);
            }
        }
    }
}
