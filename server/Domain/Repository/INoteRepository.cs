﻿namespace Domain.Repository
{
    using System.Linq;
    using Domain.Models;

    public interface INoteRepository
    {
        IQueryable<Note> GetNotes();

        Note GetNote(int id);

        Note InsertNote(Note note);

        Note UpdateNote(Note note);

        void DeleteNote(int id);
    }
}
