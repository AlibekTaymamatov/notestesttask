﻿namespace Domain.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class Note
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string TitleNote { get; set; }

        [Required]
        public DateTime DateOfCreation { get; set; }

        [MaxLength(200)]
        public string Description { get; set; }
    }
}
