﻿namespace CleanArchitecture.Infra.Data.Repositories
{
    using System;
    using System.Linq;
    using Domain.Models;
    using Domain.Repository;
    using Infrastructure.EF;
    using Microsoft.EntityFrameworkCore;

    public class NoteRepository : INoteRepository
    {
        private DatabaseContext context;

        public NoteRepository(DatabaseContext context)
        {
            this.context = context;
        }

        public Note InsertNote(Note note)
        {
            var entity = context.Add(note);
            context.SaveChanges();
            return entity.Entity;
        }

        IQueryable<Note> INoteRepository.GetNotes()
        {
            return context.Notes.AsNoTracking();
        }

        public Note GetNote(int id)
        {
            return context.Notes.Find(id);
        }

        public Note UpdateNote(Note newNote)
        {
            Note note = GetNote(newNote.Id);

            if (note is null)
            {
                return null;
            }

            note.TitleNote = newNote.TitleNote;
            note.Description = newNote.Description;
            note.DateOfCreation = newNote.DateOfCreation;
            context.SaveChanges();

            return note;
        }

        public void DeleteNote(int id)
        {
            try
            {
                var remoteObject = context.Notes.Find(id);
                context.Notes.Remove(remoteObject);
                context.SaveChanges();
            }
            catch
            {
                throw new ArgumentNullException();
            }
        }
    }
}
